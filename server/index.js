import express from 'express';
import http from 'http';
import { Server } from 'socket.io';

import cors from 'cors';
import morgan from 'morgan';

const PORT = 5000;
const app = express();
const server = http.createServer(app);

// list connected users
const usersList = [];

// origin: url client
const io = new Server(server, {
  cors: { origin: 'http://localhost:3000' },
});

/************* MIDDLEWARE *************/
// use cors for connect any IP
// app.use(cors());

// morgan HTTP logger
app.use(morgan(':method :url :status :res[content-length] :response-time ms'));

// use json format data from web-client
app.use(express.json());
/**************************************/

// Routes
app.get('/', (res, req) => {
  req.json({ message: 'hello' });
});

// start Socket server. Listen 'connection' event
io.on('connection', (socket) => {
  console.log(`${socket.id} user connected`);

  // get message from client
  socket.on('message', (data) => {
    console.log('message-server', data);
    io.emit('response', data);
  });

  socket.on('newUser', (data) => {
    usersList.push(data);
    console.log(usersList);
    io.emit('responseNewUser', usersList);
  });

  socket.on('logout', (data) => {
    console.log(`${data.user} user logout`);

    const index = usersList.findIndex((elem) => elem.user === data.user);
    if (index !== -1) {
      usersList.splice(index, 1);
    }
    io.emit('responseNewUser', usersList);
  });

  // disconnect user
  socket.on('disconnect', (data) => {
    console.log(`${socket.id} user disconnect`);
  });
});

// start HTTP server
const start = () => {
  try {
    server.listen(PORT, () => {
      console.log(`*** Server started on port: ${PORT} ***`);
    });
  } catch (error) {
    console.log(error);
  }
};
start();
