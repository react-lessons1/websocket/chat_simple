import { Outlet } from 'react-router-dom';
import { io } from 'socket.io-client';
import { Menu } from './Menu';
import { useState } from 'react';

// url server
const socket = io('http://localhost:5000');

export const App = () => {
  // set user name
  const [user, setUser] = useState('');

  return (
    <>
      <div>
        <Menu />
      </div>
      <div>
        {/* set props for context */}
        <Outlet context={{ socket, user, setUser }} />
      </div>
    </>
  );
};
