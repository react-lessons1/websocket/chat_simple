import { useOutletContext } from 'react-router-dom';
import { Sidebar } from './components/Sidebar';
import { Messages } from './components/Messages';
import { Body } from './components/Body';

import styles from './styles.module.css';
import { useEffect, useState } from 'react';

export const Chat = () => {
  // get props from context
  const { socket, user, setUser } = useOutletContext();

  const [messages, setMessages] = useState([]);

  useEffect(() => {
    socket.on('response', (data) => {
      setMessages([...messages, data]);
    });
  }, [messages, socket]);

  console.log('chat', user);
  return (
    <>
      <h3>Hello {user}</h3>
      <div className={styles.chat}>
        <Sidebar user={user} setUser={setUser} socket={socket} />
        <div className={styles.main}>
          <Body messages={messages} user={user} />
          <Messages socket={socket} user={user} />
        </div>
      </div>
    </>
  );
};
