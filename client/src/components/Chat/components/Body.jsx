import styles from 'src/components/Chat/styles.module.css';

export const Body = ({ messages, user }) => {
  return (
    <div className={styles.container}>
      {messages.map((elem) => {
        if (elem.name === user) {
          return (
            <div
              key={elem.id}
              className={`${styles.chats} ${styles.chatSender}`}
            >
              <div className={styles.userName}>You:</div>
              <div className={styles.messageSender}>
                <p>{elem.text}</p>
              </div>
            </div>
          );
        }
        return (
          <div
            key={elem.id}
            className={`${styles.chats} ${styles.chatRecipient}`}
          >
            <div className={styles.userName}>{elem.name}</div>
            <div className={styles.messageRecipient}>
              <p>{elem.text}</p>
            </div>
          </div>
        );
      })}
    </div>
  );
};
