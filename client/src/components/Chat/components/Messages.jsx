import styles from 'src/components/Chat/styles.module.css';
import { useState } from 'react';

export const Messages = ({ socket, user }) => {
  const [message, setMessage] = useState('');

  const handleSend = (e) => {
    e.preventDefault();

    // send message for server
    if (message.trim() && user) {
      socket.emit('message', {
        text: message,
        name: user,
        id: Math.floor(Math.random() * (100000 - 1 + 1)) + 1,
        socketId: socket.id,
      });
    }

    setMessage('');
  };

  return (
    <div className={styles.messageBlock}>
      <form className={styles.formSendMessage} onSubmit={handleSend}>
        <input
          type="text"
          className={styles.userMessage}
          value={message}
          onChange={(e) => setMessage(e.target.value)}
        />
        <button type="submit">send</button>
      </form>
    </div>
  );
};
