import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import styles from 'src/components/Chat/styles.module.css';

export const Sidebar = ({ socket, user, setUser }) => {
  const navigate = useNavigate();

  const [usersList, setUsersList] = useState([]);

  useEffect(() => {
    socket.on('responseNewUser', (data) => {
      setUsersList(data);
    });
  }, [usersList, socket]);

  const handleClick = () => {
    socket.emit('logout', { user, socketId: socket.id });
    setUser('');
    navigate('/');
  };
  return (
    <div className={styles.sidebar}>
      <header className={styles.bodyHeader}>
        <button className={styles.btn} onClick={handleClick}>
          leave chat
        </button>
      </header>
      <h4>Users:</h4>
      <ul className={styles.users}>
        {usersList.map((elem) => (
          <li key={elem.socketId}>{elem.user}</li>
        ))}
      </ul>
    </div>
  );
};
