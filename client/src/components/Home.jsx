import { useNavigate, useOutletContext } from 'react-router-dom';

export const Home = () => {
  const { socket, user, setUser } = useOutletContext();
  const navigate = useNavigate();

  console.log('home', user);

  const handleSubmit = (e) => {
    e.preventDefault();
    socket.emit('newUser', { user, socketId: socket.id });
    navigate('/chat');
  };
  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        value={user}
        onChange={(e) => {
          setUser(e.target.value);
        }}
        placeholder="user"
      />
      <button type="submit">enter</button>
    </form>
  );
};
