import { NavLink } from 'react-router-dom';

export const Menu = () => {
  return (
    <>
      <ul>
        <li>
          <NavLink to="/">Home</NavLink>
        </li>
        <li>
          <NavLink to="/chat">Chat</NavLink>
        </li>
        <hr />
      </ul>
    </>
  );
};
