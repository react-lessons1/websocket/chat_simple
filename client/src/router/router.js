import { createBrowserRouter } from 'react-router-dom';
import { App } from 'src/components/App';
import { Chat } from 'src/components/Chat/Chat';
import { Home } from 'src/components/Home';

export const router = createBrowserRouter([
  {
    element: <App />,
    path: '/',
    errorElement: <div>error url</div>,
    children: [
      {
        element: <Home />,
        index: true,
      },
      {
        element: <Chat />,
        path: '/chat',
      },
    ],
  },
]);
